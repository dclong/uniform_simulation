library(dclong.fs)
library(inline)
library(Rcpp)
path = "uniform.cpp"
includes = readText(title=path)

settings=getPlugin("Rcpp")
settings$env$PKG_CXXFLAGS=paste(' -std=c++11 ',settings$env$PKG_CXXFLAGS)

fx = cxxfunction(signature(), body="", includes=includes, settings=settings)

mod = Module( "mod" , getDynLib(fx) , mustStart=T)
print(mod)
