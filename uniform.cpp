#include <random>
#include <vector>
#include <iostream>
#include <chrono>


std::mt19937_64 rng;
std::uniform_real_distribution<> urd;

double rlastu1(double upper){
    double current = urd(rng);
    double s = current;
    while(s <= upper){
        current = urd(rng);
        s += current;
    }
    return current;
}
/** 
 * @param n number of the last (hiting) uniform random variables 
 * to be generated.
 *
 * @param upper the (upper) limit (accumulating from 0) to hit.
 *
 * @return a vector of double values representing a sample
 * from the distribution of the las (hiting) unifrom random variable.
 */
std::vector<double> rlastu(long n, double upper){
#ifndef NORCPP
    RNGScope scope;
#endif
    std::vector<double> sample(n);
    for(auto it=std::begin(sample); it!=std::end(sample); ++it){
        *it = rlastu1(upper);
    }
    return sample;
}

bool second_person_wins(){
    double last1 = urd(rng);
    double s = last1;
    while(s <= 1){
        last1 = urd(rng);
        s += last1;
    }
    double last2 = urd(rng);
    s += last2;
    while(s <= 2){
        last2 = urd(rng);
        s += last2;
    }
    return last2 > last1;
}

double p2(long sim_size){
#ifndef NORCPP
    RNGScope scope;
#endif
   long s = 0;
   for(long i=0; i<sim_size; ++i){
       s += second_person_wins();
   }
   return s / (double) sim_size;
}

#ifndef NORCPP
RCPP_MODULE(mod){
    function("rlastu", &rlastu);
    function("p2", &p2);
}
#else
int main(int argc, char *argv[]){
    //auto sample = rlastu(100, 1);
    auto starting_time = std::chrono::system_clock::now();
    double p = p2(std::atol(argv[1]));
    auto ending_time = std::chrono::system_clock::now();
    auto seconds = (double) (ending_time - starting_time).count() / 1E9;
    std::cout << "The probablity for the second person to win is: " << p << std::endl
        << "The simulation took " << seconds << " seconds." << std::endl;
    return 0;
}
#endif

